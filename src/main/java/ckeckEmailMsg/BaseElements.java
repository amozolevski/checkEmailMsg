package ckeckEmailMsg;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BaseElements {

    public BaseElements(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    //Email input field
    @FindBy (xpath = "//input[@type='email']")
    public WebElement emailInput;

    //'Next' button
    @FindBy (xpath = "//*[@id='identifierNext']")
    public WebElement nextButton;

    //Password input field
    @FindBy (xpath = "//input[@type='password']")
    public WebElement passwordInput;

    //'Next' after password button
    @FindBy (xpath = "//*[@id='passwordNext']")
    public WebElement nextPassButton;

    ////////Main/Inbox Page////////////

    //Inbox button
    @FindBy(xpath = "//a[contains(@href, 'https://mail.google.com/mail/u/0/#inbox')]")
    public WebElement inboxButton;

    ////////LogOut buttons////////////

    @FindBy (xpath = "//a[contains(@href, 'SignOutOptions')]")
    public WebElement signOutOptions;

    @FindBy (xpath = "//a[contains(@href, 'Logout')]")
    public WebElement logOutButton;

}