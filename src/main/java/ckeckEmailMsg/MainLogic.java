package ckeckEmailMsg;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainLogic {

    public WebDriver driver;
    public WebDriverWait wait;
    public BaseElements elements;

    public MainLogic(WebDriver driver, WebDriverWait wait, BaseElements elements) {
        this.driver = driver;
        this.wait = wait;
        this.elements = elements;
    }

    public void login(){
        //wait Email input isDisplayed
        waitForVisible(elements.emailInput).sendKeys(Variables.EMAIL);
        elements.nextButton.click();

        //wait Password input isDisplayed
        waitForVisible(elements.passwordInput).sendKeys(Variables.PASSWORD);
        elements.nextPassButton.click();

        //wait until redirect to Inbox
        wait.until((WebDriver a) -> a.getCurrentUrl().equals("https://mail.google.com/mail/u/0/#inbox"));
    }

    //wait for visibility of an element
    public WebElement waitForVisible(WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    //logOut
    public void logOut() {
        //press inbox
        waitForVisible(elements.inboxButton).click();

        //press signOut button
        waitForVisible(elements.signOutOptions).click();

        ////press logOut button
        waitForVisible(elements.logOutButton).click();

        //press pop-up alert if present
        try{
            driver.switchTo().alert().accept();
        }catch (NoAlertPresentException napex){
            System.out.println("no alert " + napex.getMessage());
        }

        //wait redirect to signIn page
        wait.until((WebDriver a) -> a.getCurrentUrl().contains("https://accounts.google.com/"));
    }
}