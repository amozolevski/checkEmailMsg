package ckeckEmailMsg.elements;

import ckeckEmailMsg.BaseElements;
import ckeckEmailMsg.Variables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InboxElements extends BaseElements {

    public InboxElements(WebDriver driver) {
        super(driver);
    }

    //element with 'SUBJECT'
    @FindBy (xpath = "//td/div[@role='link']//span[text()='"+Variables.SUBJECT +"']")
    public WebElement subject;

    //element with 'from'
    @FindBy (xpath = "//span[@class='qu']/span[@email='"+Variables.EMAIL+"']")
    public WebElement fromField;

    //element with message body. Please note: the 1st line needs only! (e.g. "test message")
    @FindBy (xpath = "//div/div[contains(text(), '"+Variables.MESSAGE+"')]")
    public WebElement messageText;

}