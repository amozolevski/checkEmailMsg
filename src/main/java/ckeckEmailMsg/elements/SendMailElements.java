package ckeckEmailMsg.elements;

import ckeckEmailMsg.BaseElements;
import ckeckEmailMsg.Variables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SendMailElements extends BaseElements {

    public SendMailElements(WebDriver driver) {
        super(driver);
    }

    //new email button
    @FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
    public WebElement newEmailButton;

    //Form 'new message'
    @FindBy(xpath = "//div[@class='aYF']")
    public WebElement newMsgForm;

    //recipients input
    @FindBy(xpath = "//textarea[@class='vO']")
    public WebElement recipientInput;

    //SUBJECT input
    @FindBy(xpath = "//input[@name='subjectbox']")
    public WebElement subjecttInput;

    //text of the message
    @FindBy(xpath = "//div[@role='textbox']")
    public WebElement textMsg;

    //send button
    @FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO T-I-atl L3']")
    public WebElement sendButton;

    //element with 'SUBJECT'
    @FindBy (xpath = "//td/div[@role='link']//span[text()='"+Variables.SUBJECT +"']")
    public WebElement newMsgSubject;

}
