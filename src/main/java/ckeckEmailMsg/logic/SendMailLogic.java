package ckeckEmailMsg.logic;

import ckeckEmailMsg.MainLogic;
import ckeckEmailMsg.Variables;
import ckeckEmailMsg.elements.SendMailElements;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SendMailLogic extends MainLogic {

    public SendMailElements elements;

    String sSubject;

    public SendMailLogic(WebDriver driver, WebDriverWait wait, SendMailElements elements) {
        super(driver, wait, elements);
        this.elements = elements;
    }

    public String getSubject() {
        return sSubject;
    }

    public void sendMail(){

        //new mail button
        waitForVisible(elements.newEmailButton).click();

        //visible new message form
        waitForVisible(elements.newMsgForm);

        //fill recipients
        waitForVisible(elements.recipientInput).click();
        elements.recipientInput.sendKeys(Variables.EMAIL);
        elements.recipientInput.sendKeys(Keys.ENTER);

        //fill SUBJECT
        waitForVisible(elements.subjecttInput).click();
        elements.subjecttInput.sendKeys(Variables.SUBJECT);

        //fill text field
        waitForVisible(elements.textMsg).click();
        elements.textMsg.sendKeys(Variables.MESSAGE);

        //press send button
        waitForVisible(elements.sendButton).click();

        //wait new email with the same 'SUBJECT'
        sSubject = waitForVisible(elements.newMsgSubject).getText();

        //press inbox button
        waitForVisible(elements.inboxButton).click();
    }
}
