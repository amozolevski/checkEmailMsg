package ckeckEmailMsg.logic;

import ckeckEmailMsg.MainLogic;
import ckeckEmailMsg.elements.InboxElements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class inboxMailLogic extends MainLogic {

    private InboxElements elements;

    private String messageText;

    public inboxMailLogic(WebDriver driver, WebDriverWait wait, InboxElements elements) {
        super(driver, wait, elements);
        this.elements = elements;
    }

    public String getMessageText() {
        return messageText;
    }

    public void checkEmail(){
        //press 'Inbox' button
        waitForVisible(elements.inboxButton).click();

        //click on 'SUBJECT' field of needed email message
        waitForVisible(elements.subject).click();

        //wait for 'From' field
        waitForVisible(elements.fromField);

        //get message text
        messageText = waitForVisible(elements.messageText).getText();

        //back to Inbox
        waitForVisible(elements.inboxButton).click();
    }
}