package checkEmailMsgTest;

import ckeckEmailMsg.BaseElements;
import ckeckEmailMsg.MainLogic;
import ckeckEmailMsg.Variables;
import ckeckEmailMsg.elements.InboxElements;
import ckeckEmailMsg.elements.SendMailElements;
import ckeckEmailMsg.logic.SendMailLogic;
import ckeckEmailMsg.logic.inboxMailLogic;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class checkEmailMsgTest {

    private WebDriver driver;
    private MainLogic mainLogic;
    private inboxMailLogic inboxMailLogic;
    private InboxElements inboxElements;

    private SendMailElements sendMailElements;
    private SendMailLogic sendMailLogic;

    @BeforeClass
    void beforeTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                DesiredCapabilities.chrome());
        driver.get("https://accounts.google.com/AccountChooser?service=mail&amp;continue=https://mail.google.com/mail");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        BaseElements elements = new BaseElements(driver);

        mainLogic = new MainLogic(driver, wait, elements);
        inboxElements = new InboxElements(driver);
        inboxMailLogic = new inboxMailLogic(driver, wait, inboxElements);

        sendMailElements = new SendMailElements(driver);
        sendMailLogic = new SendMailLogic(driver, wait, sendMailElements);
    }

    @Test
    public void test_01_Login(){
        mainLogic.login();
        Assert.assertEquals(driver.getCurrentUrl(), "https://mail.google.com/mail/u/0/#inbox");
    }

    @Test
    public void test_02_sendMail(){
        sendMailLogic.sendMail();
        String currentSubject = sendMailLogic.getSubject();
        String expectedSubject = Variables.SUBJECT;
        Assert.assertEquals(currentSubject, expectedSubject);
    }

    @Test
    public void test_03_CheckEmailMsg(){
        inboxMailLogic.checkEmail();
        String currentText = inboxMailLogic.getMessageText();
        String expectedText = Variables.MESSAGE;
        Assert.assertEquals(currentText, expectedText);
    }

    @Test
    public void test_04_LogOut(){
        mainLogic.logOut();
        Assert.assertEquals(driver.getCurrentUrl().contains("https://accounts.google.com"), true);
    }

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}